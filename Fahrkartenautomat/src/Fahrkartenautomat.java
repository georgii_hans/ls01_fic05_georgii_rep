﻿import java.util.Scanner;

class Fahrkartenautomat
{
    public static void main(String[] args)
    {   
    	while (true) {
    	double einzelpreis = ticketAuswahlErfassen();      	
    	
    	int AnzahlDerTickets = anzahlDerTicketsErfassen();
    	
    	double zuZahlenderBetrag = fahrkartenbestellungErfassen(einzelpreis,AnzahlDerTickets);
    	
    	double rückgabebetrag = fahrkartenBezahlen(zuZahlenderBetrag); 
    	   	
    	fahrkartenAusgeben(rückgabebetrag);
    	}
    }
    
    public static double fahrkartenbestellungErfassen(double einzelpreis, int AnzahlDerTickets) {
    	double zuZahlenderBetrag = einzelpreis * AnzahlDerTickets;
    	return zuZahlenderBetrag;
    }
    
    public static void ticketPreise() {
    	System.out.println("Wählen Sie ihre Wunschfahrkarte für Berlin AB aus:");
        System.out.println("  Einzelfahrschein Regeltarif AB [2,90 EUR] (1)");
        System.out.println("  Tageskarte Regeltarif AB [8,60 EUR] (2)");
        System.out.println("  Kleingruppen-Tageskarte Regeltarif AB [23,50 EUR] (3)");
    }
    
    public static double ticketAuswahlErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
        ticketPreise();
    	
    	int ticketAuswahl = tastatur.nextInt();
    	
        if (ticketAuswahl == 1) {
        	System.out.println("Ihre Wahl: " + ticketAuswahl); 
        	return 2.9;
        }
        else if (ticketAuswahl == 2) {
        	System.out.println("Ihre Wahl: " + ticketAuswahl); 
        	return 8.6;
        }
        else if (ticketAuswahl == 3) {
        	System.out.println("Ihre Wahl: " + ticketAuswahl); 
        	return 23.50;
        }
        else {
        	falscheMeldung();      	
        	return ticketAuswahlErfassen();        	
        }
    }   
    
    public static int anzahlDerTicketsErfassen() {
    	Scanner tastatur = new Scanner(System.in);
    	
    	System.out.print("Bitte geben Sie die Anzahl der Tickets (zwischen 1 und 10): ");
    	
    	int anzahl = tastatur.nextInt();
    	
    	if (anzahl >= 1 && anzahl <= 10) {
    		return anzahl; 
    	}
    	else {
    		falscheMeldung();        	
        	return anzahlDerTicketsErfassen();   		
    	}
    }    
    
    public static void falscheMeldung() {
    	System.out.println(">>Falsche Eingabe<<");
    }
    
    public static double fahrkartenBezahlen(double zuZahlenderBetrag) {
    	Scanner tastatur = new Scanner(System.in);
    	
    	double eingezahlterGesamtbetrag;
    	double eingeworfeneMünze;
    	double rückgabebetrag;
    	
    	eingezahlterGesamtbetrag = 0.0f;
        while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
        {
     	   System.out.printf("Noch zu zahlen: %.2f Euro\n", (zuZahlenderBetrag - eingezahlterGesamtbetrag));
     	   System.out.print("Eingabe (mind. 5Ct, höchstens 2 Euro): ");
     	   eingeworfeneMünze = tastatur.nextDouble();
           eingezahlterGesamtbetrag += eingeworfeneMünze;
        }        
                
        rückgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;        
                
        return rückgabebetrag;    	
    }
    
    public static void fahrkartenAusgeben(double rückgabebetrag) {
    	
    	System.out.println("\nFahrschein wird ausgegeben");
        warte(250);
        
        rueckgeldAusgeben(rückgabebetrag);
        
        System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                "vor Fahrtantritt entwerten zu lassen!\n"+
                "Wir wünschen Ihnen eine gute Fahrt.\n");
    }
    
    public static void rueckgeldAusgeben(double rückgabebetrag) {
    	if(rückgabebetrag > 0.0)
        {
     	   System.out.printf("Der Rückgabebetrag in Höhe von %.2f Euro ", rückgabebetrag);
     	   System.out.println("wird in folgenden Münzen ausgezahlt:");

            while(rückgabebetrag >= 2.0f) // 2 EURO-Münzen
            {
         	  System.out.println("2 EURO");
 	          rückgabebetrag -= 2.00f;
            }
            while(rückgabebetrag >= 1.0) // 1 EURO-Münzen
            {
         	  System.out.println("1 EURO");
 	          rückgabebetrag -= 1.00f;
            }
            while(rückgabebetrag >= 0.5) // 50 CENT-Münzen
            {
         	  System.out.println("50 CENT");
 	          rückgabebetrag -= 0.50f;
            }
            while(rückgabebetrag >= 0.2) // 20 CENT-Münzen
            {
         	  System.out.println("20 CENT");
  	          rückgabebetrag -= 0.20f;
            }
            while(rückgabebetrag >= 0.1) // 10 CENT-Münzen
            {
         	  System.out.println("10 CENT");
 	          rückgabebetrag -= 0.10f;
            }
            while(rückgabebetrag >= 0.05)// 5 CENT-Münzen
            {
         	  System.out.println("5 CENT");
  	          rückgabebetrag -= 0.05f;
            }
        }
    }
    
    public static void warte(int millisekunde) {
    	for (int i = 0; i < 8; i++)
        {
           System.out.print("=");
	           try {
	 			Thread.sleep(millisekunde);
	 		} catch (InterruptedException e) {
	 			// TODO Auto-generated catch block
	 			e.printStackTrace();
	 		}
        }
        System.out.println("\n\n");
    }
}