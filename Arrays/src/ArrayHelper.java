
public class ArrayHelper {

	public static void main(String[] args) {
		int[] zahlen = {1,2,3,4,5};
		System.out.print(convertArrayToString(zahlen));

	}
	
	public static String convertArrayToString(int[] zahlen) {
		String convertedArray = "";
		
		for (int i = 0; i < zahlen.length; i++) {
			if (zahlen[i] == zahlen[(zahlen.length - 1)]) {
				convertedArray += zahlen[i];						
			} else {
				convertedArray += zahlen[i] + ",";						

			}
			
		}
		
		return convertedArray;
	}
}
