
public class Lotto {

	public static void main(String[] args) {
		int[] lottoArr = { 3,7,12,18,37,42 };
		
		System.out.print("[ ");		
		for (int lottoZahl : lottoArr ) {
			System.out.print(lottoZahl + " ");
		}		
		System.out.println("]");
		
		/// Check for 12		
		for (int lottoZahl : lottoArr ) {
			if (lottoZahl == 12)  {
				System.out.println(lottoZahl + " kommt in der Lottoziehung vor.");
				break;
			}
		}	
		
		/// Check for 13
		for (int lottoZahl : lottoArr ) {
			if (lottoZahl == 13)
				System.out.println(lottoZahl + " kommt in der Lottoziehung vor.");
				break;
		}
	}

}
