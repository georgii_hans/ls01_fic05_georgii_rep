import java.util.Scanner;

public class Mathe {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Geben Sie bitte einen Zahl:");
		double zahl = myScanner.nextDouble();
		
		double quadrat = quadrat(zahl);
		
		System.out.printf("Der Quadrat Ihrer Zahl ist: %.2f%n", quadrat);

	}
	
	public static double quadrat(double x) {
		x *= x;
		return x;
	}
}
