import java.util.Scanner;

public class PCHaendler {

	public static void main(String[] args) {

		// Benutzereingaben lesen
		System.out.println("was moechten Sie bestellen?");
		String artikel = liesString();

		System.out.println("Geben Sie die Anzahl ein:");
		int anzahl = liesInt();

		System.out.println("Geben Sie den Nettopreis ein:");
		double preis = liesDouble();

		System.out.println("Geben Sie den Mehrwertsteuersatz in Prozent ein:");
		double mwst = liesMwst();

		// Verarbeiten
		double nettogesamtpreis = berechneGesamtnettopreis(anzahl, preis);
		double bruttogesamtpreis = berechneGesamtbruttopreis(nettogesamtpreis, mwst);

		// Ausgeben

		rechungausgeben(artikel, anzahl, nettogesamtpreis, bruttogesamtpreis, mwst);
	}
		
	public static String liesString() {
		Scanner myScanner = new Scanner(System.in);
		String artikel = myScanner.next();
		return artikel;
	}
	
	public static int liesInt() {
		Scanner myScanner = new Scanner(System.in);
		int anzahl = myScanner.nextInt();
		return anzahl;
	}
	
	public static double liesDouble() {
		Scanner myScanner = new Scanner(System.in);
		double preis = myScanner.nextDouble();
		return preis;
	}
	
	public static double liesMwst() {
		Scanner myScanner = new Scanner(System.in);
		double mwst = myScanner.nextDouble();
		return mwst;
	}
	
	public static void rechungausgeben(
		String artikel, 
		int anzahl, 
		double nettogesamtpreis, 
		double bruttogesamtpreis, 
		double mwst
		) {
				
		System.out.println("\tRechnung");
		System.out.printf("\t\t Netto:  %-20s %6d %10.2f %n", artikel, anzahl, nettogesamtpreis);
		System.out.printf("\t\t Brutto: %-20s %6d %10.2f (%.1f%s)%n", artikel, anzahl, bruttogesamtpreis, mwst, "%");
		
	}
	
	public static double berechneGesamtnettopreis(int anzahl, double nettopreis) {
		double nettogesamtpreis = anzahl * nettopreis;
		return nettogesamtpreis;
	}
		
	public static double berechneGesamtbruttopreis(double nettogesamtpreis, double mwst) {
		double bruttogesamtpreis = nettogesamtpreis * (1 + mwst / 100);
		return bruttogesamtpreis;
	}
}