import java.util.Scanner;

public class Widerstand {

	public static void main(String[] args) {
		Scanner myScanner = new Scanner(System.in);
		
		System.out.println("Bitte erste Widerstand eingeben:");
		double widerstand_1 = myScanner.nextDouble();
		
		System.out.println("Bitte zweite Widerstand eingeben:");
		double widerstand_2 = myScanner.nextDouble();		
		
		double reihenschaltung_widerstand = reihenschaltung(widerstand_1, widerstand_2);	
		
		double parallelschaltung_widerstand = parallelschaltung(widerstand_1, widerstand_2);
		
		System.out.printf("Die Reihenschaltungwiderstand ist %.2f Ohm %n", reihenschaltung_widerstand);
		System.out.printf("Die Parallelschaltungwiderstand ist %.2f Ohm %n", parallelschaltung_widerstand);
	}
	
	public static double reihenschaltung(double r1, double r2) {
		
		double reihenschaltung = r1 + r2;
		
		return reihenschaltung;
		
	}
	
	public static double parallelschaltung(double r1, double r2) {
		double parallelschaltung = (1/r1) + (1/r2);
		
		return parallelschaltung;
	}
}
