
public class Aufgabe_2 {
	
	// Recursive function that calculates factorial.
	static int fakultät(int n){    
		if (n == 0)    
		    return 1;    
		  else    
		    return(n * fakultät(n-1));    
	}
	
	// Three functions that calculate the strings that will be shown.
	static String ersteTeilBearbeiten(int n) {
		String ersteTeil = String.valueOf(n);
		ersteTeil += "!";
			
		return ersteTeil;
	}
		
	static String zweiteTeilBearbeiten(int n) {
		String zweiteTeil = "";
		
		for  (int i=1;i<=n;i++) {
			zweiteTeil += String.valueOf(i);
			
			if (i!=n) {
			zweiteTeil += " * ";
			}
		}
		
		return zweiteTeil;		
	}
		
	static String dritteTeilBearbeiten(int n) {
		int i = fakultät(n);
				
		String dritteTeil = String.valueOf(i);
		
		return dritteTeil;
	}
	
	// This functions prints out the table based on an integer passed to it.
	static void fakultätTabelle(int n) {
		for (int i=0;i<=n;i++){
			String ersteTeil = ersteTeilBearbeiten(i);
			String zweiteTeil = zweiteTeilBearbeiten(i);
			String dritteTeil = dritteTeilBearbeiten(i);
			
			System.out.printf("%-5s= %-19s =%4s \n", ersteTeil, zweiteTeil, dritteTeil);  
		}
	}
	
	public static void main(String[] args) {
		
		// Here I call the function.
		fakultätTabelle(5);
		
			
		}
}
