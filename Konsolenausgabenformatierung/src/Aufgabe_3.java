
public class Aufgabe_3 {
	public static void main(String[] args) {
		String celsius = "Celsius";
		String fahrenheit = "Fahrenheit";
		String linie = "------------------------";
		
		System.out.printf("%-12s|%10s\n", fahrenheit, celsius);
		System.out.printf("%s\n", linie);
		System.out.printf("%+-12d|%10.2f\n", -21, -28.8889);
		System.out.printf("%+-12d|%10.2f\n", -10, -23.3333);
		System.out.printf("%+-12d|%10.2f\n",  +0, -17.7778);
		System.out.printf("%+-12d|%10.2f\n",  +20, -6.6667);
		System.out.printf("%+-12d|%10.2f\n",  +30, -1.1111);
	}
}
